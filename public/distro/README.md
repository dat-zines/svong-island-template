# THE DISTRO FOLDER 

Distros are an essential aspect of zine culture.  Distros collect and distribute zines.  They are often run by a small amount of people (or just one person), and are mythically important but materially small (the distro that changes your life may just be a box in a closet of a rented room, and run by one passionate zinester.)

In the same way, customizing your dat zine on your own computer is delightful, but limiting.  You want to _share_ your work too, and give guidance on how it's shared.

The `distro` folder is intended to give instructions for when this zine is found and shared by dat-zine distros and libraries.  It contains an info.txt page and a cover image (title cover.jpg, or cover.gif, or cover.png).

The info.txt file is formatted just like the poem.zine file, except that you get to decide what all the sections are called.  So you could have something like:

```
title: My Poem Zine
----
sharing: Share this among your friend, but not on social media.
----
songs i listened to:
 - a
 - list of
 - songs
```

Or it could look like

```
poem: my poem title
----
inspirations: I was inspired by park benches and on-time busses.
----
location: I wrote this at the botanical garden, on an especially sunny and smooth day.
```

As long as it follows that four dash format, yr good!

## What _should_ I put in my info.txt

While it can be anything, what it _should_ be is based on the community you're sharing it within!  They may want to gather poetry zines based around themes, or inspirations, or colors.  So you might want to include any of htose aspects in your info text to help the zine curator better organize their distro/library.  Or licensing and rights might be most important,a nd so the creation date, the author, contact info, and license information may ask to be included.  The important part is that this is being decided by the community as it forms, not by the software or any platform.

## Examples of Real-Life Zine Distros
My absolute favorite distro is [Antiquated Futures](http://www.antiquatedfuture.com/) AND they have a great link list of other distros.  You shoudl really just check out that!
[AF's list of Zine Distros](http://www.antiquatedfuture.com/links/)

## Examples of Dat Zine Distros or Libraries
Right now, the only distros I know are libraries--since there isn't really any payment that needs to happenw tih the zines, and you don't have to order them...so distro/library is kinda one and the same. 

My library is here: dat://zine-library.coolguy.website
Another one, that's starting up, is here: dat://zines.solarpunk.cool

BUT!  Since the libraries are _also_ on dat, it means you can make a copy of them and start your own. 

In fact, PLEASE DO!  If you want a poetry library, please start one!  And let me know the address or how I can help!
