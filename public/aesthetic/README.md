# A E S T H E T I C

This be the folder for all the styling of yr zine.  CSS is incredible, and you can change the look and feel of your zine using _nothing but_ CSS.  This is a language that continually gives back, so I definitely recommend diving into this as much as you can!

However, you may not want to get deep into CSS code right now, and just wanna adjust the background color or font or what-have-you.  To help with this, we have a tailored 'colors-and-fonts' stylesheet that lets you quickly switch these around.

## Navigating this Folder

### Colors-and-fonts.css
The first file you want to check out is `colors-and-fonts.css`.  There's more helpful comments within that file, but essentially you'll edit it like you edited your poem.zine, replacing the color and font options for each thing with your own choice.

For example, the background of the poem is currently set to #FFFCEB, a nice yellow cream of a color.  But maybe you want your poem to be set against a pale blue.

To do this, you'd go into colors-and fonts and look for this line:
`--primary_bg: #FFFCEB;` and change it to `--primary_bg: #f6fffe;`

Then, press save!

### Main.css

This contains the full on stylesheet for the zine.  This is a pretty simple zine, so there isn't much there.  But you can edit and adjust this however you'd like.  My recommendation for this style of zine is to add additional values to each existing section, rather than trying to make new class names or what-have-you. 

## Resources
Mozilla Dev Network has a good guide to css:
https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS

Another great resource is css-tricks:
https://css-tricks.com/guides/beginner/

Both of these are forever resources, like you'll find new and valuable things as you grow as a code zinester.  If there's stuff that seems intimidating or strnage within the guides, it's just cos it's strange _right now_.  Future you will be all about it.
