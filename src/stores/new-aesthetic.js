import { writable, derived } from 'svelte/store';
import { newSongIsland } from './new-songIsland.js';
import rgbhex from 'rgb-hex';
import hexrgb from 'hex-rgb'

//given an RGB object, and a chosen opacity, return an rgba string that's CSS ready.
const createRGBA = (rgb, opacity) => `rgba(${rgb.red}, ${rgb.green}, ${rgb.blue}, ${opacity})`

export const aesthetic = writable({
  accentColor: '',
  backgroundColor: '',
  picture: '',
  backgroundPosition: '',
  backgroundSize: '',
  color: '',
  gradient: {
    top: '#000000',
    bottom: '#ffffff',
    opacity: '0.5'
  },
  primaryColor: ''
})

export const gradientRGBs = derived(
  aesthetic,
  ($aesthetic, set) => {
    set({
      top: hexrgb($aesthetic.gradient.top),
      bottom: hexrgb($aesthetic.gradient.bottom),
      opacity: $aesthetic.gradient.opacity
    })
  }
)

export const gradientDisplay = derived(
  gradientRGBs,
  ($g, set) => {
    let top = createRGBA($g.top, $g.opacity)
    let bottom = createRGBA($g.bottom, $g.opacity)
    set(`linear-gradient(${top}, ${bottom})`)
  }
)

// rgba colors with less opacity don't play
// well with our hex converter, so we
// make them into a nicer rgb, removing
// the last value.
function properRGB (rgba) {
  let vals = rgba.replace(/(rgba\()|(\))/g,'').split(',')
  return `rgb(${vals[0]}, ${ vals[1] }, ${ vals[2] })`
}

document.addEventListener('DOMContentLoaded', () => {
  // We'll check the current style of the page, and use that to
  // fill out the initial look in the edit.  We need to convert stuff
  // to hex, as that's the only thing the color input form will take.
  const bodyStyle = window.getComputedStyle(document.body, null);
  const backgroundColor = "#" + rgbhex(bodyStyle.backgroundColor);
  const gradientStyle = window.getComputedStyle(document.querySelector('#aesthetic-hack'));
  const color = "#" + rgbhex(bodyStyle.color);
  let gradTop = properRGB(gradientStyle.backgroundColor)
  let gradBottom = properRGB(gradientStyle.color)

  const gradient = {
    top: '#' + rgbhex(gradTop),
    bottom: '#' + rgbhex(gradBottom),
    opacity: 0.5
  };

  aesthetic.set({
    backgroundColor,
    picture: bodyStyle.backgroundImage,
    primaryColor: backgroundColor,
    accentColor: color,
    backgroundSize: bodyStyle.backgroundSize,
    backgroundPosition: bodyStyle.backgroundPosition,
    backgroundRepeat: bodyStyle.backgroundRepeat,
    color,
    gradient
  })
})
