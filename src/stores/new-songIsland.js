//  This is the store to hold people's selections within the edit page
//  When they save their edits, the new song island becomes the current song Island.
// But having it be its own store lets us ensure that their details are saved
// even if they cancel accidentally or refresh the page or whatever!

import {writable, derived} from 'svelte/store';
import { parse } from 'smarkt';
import { aesthetic } from './new-aesthetic.js';
import { songIsland } from './songIsland.js';

export const newSongIsland = writable({})

document.addEventListener('DOMContentLoaded', () => {
  fetch('/zine/song-island.zine')
    .then(response => response.text())
    .then(text => newSongIsland.set(parse(text)))
})

export const links = derived(
  newSongIsland,
  ($songIsland, set) => {
    let links = [];
    if ($songIsland.links) {
      set($songIsland.links);
    } else {
      set(links);
    }
  }
)

export const words = derived(
  newSongIsland,
  ($songIsland, set) => {
    let words = '';
    if ($songIsland.words) {
      set($songIsland.words);
    } else {
      set(words);
    }
  }
)

export const finalized = derived(
  [newSongIsland, aesthetic, songIsland],
  ([$newSongIsland, $aesthetic, $songIsland], set) => {
    let datZine = new DatArchive(window.location.href)
    set({
      newSongUploaded: $newSongIsland.file !== undefined,
      newPictureUploaded: $aesthetic.pictureFile !== undefined,
      newSongIslandWritten: $newSongIsland !== $songIsland
    })
  }
)
