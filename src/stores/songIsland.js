import { writable, derived } from 'svelte/store';
import { parse } from 'smarkt';
import marked from 'marked';

export const songIsland = writable({})

document.addEventListener('DOMContentLoaded', () => {
    fetch('/zine/song-island.zine')
        .then(response => response.text())
        .then(text => songIsland.set(parse(text)))
})

export const song = derived(
    songIsland,
    ($songIsland, set) => {
        set($songIsland.song)
    }
)

export const words = derived(
  songIsland,
  ($songIsland, set) => {
    if (!$songIsland.words) return '<p>...</p>'
    set(marked($songIsland.words));
  }
)

export const links = derived(
  songIsland,
  ($songIsland, set) => {
    if ($songIsland.links == undefined) set([]);
    set($songIsland.links);
  }
)
